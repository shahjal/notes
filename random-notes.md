### Trace Packet to Process
Use the TCP/UDP port in the packet to find the corresponding process ID (using lsof ), and then use the process ID to query the corresponding process name (using ps).
